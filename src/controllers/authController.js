const jwt = require('jsonwebtoken');
const User = require('../models/User');

function getToken(req, res, next) {
    console.log(req.email);
    const userId = req.userId;//,  { expiresIn: '10m' }
    jwt.sign( { email: req.email } , 'Maleteo',{ expiresIn: '10m' }, function(err, token){
        if(err){
            return err;
        }
        // console.log(token);
        res.redirect('http://localhost:3000/' + 'gettoken'+'/'+token)
        // res.json({errors: null, results: {token}})
    })
    //.json({errors: null, results: {token, user}})
}

function getTokenManual(req, res, next) {
    console.log('Get Token Manual');
    // console.log(req.body);
    const user = req.userId;//,  { expiresIn: '10m' }
    jwt.sign( { email: req.body.email } , 'Maleteo',{ expiresIn: '100m' }, function(err, token){
        if(err){
            return next(err);
        }
        // console.log(token);
        res.json({errors: null, results: {token, user}})
    })
    //.json({errors: null, results: {token, user}})
}

function loginActive(req, res, next){

    res.status(200).json({errors: false, results: 'Token ok'});
}


async function verifyToken(req, res, next) {
    try{
        let token;
        if (req.headers.authorization) {
            token = req.headers.authorization.split(' ')[1];
        }
        const userEmail = jwt.verify(token, 'Maleteo').email; //Get user email from the token
        // const userEmail2 = jwt.verify(token, 'Maleteo'); //Get user email from the token
        // console.log(userEmail);
        // console.log(userEmail2);
        // console.log('The user details: ', userEmail);
        if (token && userEmail) {
            const user = await User.findOne({ email: userEmail });
            req.userId = user.id;
            
            next();
        }
        else {
            res.status(401).json({errors: true, results: 'Ruta no autorizada'});
        }
    }
    catch(err){
        next(err);
    }
}

function handleErrorLogin(req, res, next){
    console.log('Checking Errors');
    // console.log(req.duplicatedKey);
    if (req.error){
        console.log('REQ_ERRORS');
        res.status(406).json({error: true, resource: 'Error in Values', results: req.error.name });
    }
    else if( req.duplicatedKey ) {
        console.log('DUPLICATED KEY');
        console.log(req.duplicatedKey);
        res.status(409).json({error: true, resource: 'Duplicated key', results:'Already exists '+ req.duplicatedKey.email })
    }
    else if( req.userNotFound ) {
        console.log("USER DOESN'T EXISTS");
        res.status(400).json({error: true, resource: 'No user', results:'The user doesnt exists '})
    }
    else{
        next(null,true)
    }

}

// const getApiAndEmit = socket => {
//     const response = new Date();
//     // Emitting a new message. Will be consumed by the client
//     socket.emit("FromAPI", response);
  
//   };



module.exports = {
    getToken,
    verifyToken,
    handleErrorLogin,
    loginActive,
    getTokenManual,
}



