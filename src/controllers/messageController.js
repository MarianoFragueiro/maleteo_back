const Message = require('../models/Message');


//Done
async function oneMessage(req, res, next) {
    try {
        const filter = {};
        if (req.query.s) {
            filter.name = new RegExp('.*' + req.query.s + '.*');
        }
        const messageList = await Message.find( { _id: req.params.messageId } ).populate(
                {
                    path: 'user',
                }
            ).populate(
                {
                    path: 'guardian'
                }
            );
        res.status(200).json({error: null, resource: 'Message', results: messageList });
    }
    catch(err) {
        next(err);
    }
}

async function oneMessageSocketIo(data) {
    try {
        console.log('socketioController: ',data);
        const messageList = await Message.find( data ).populate(
                {
                    path: 'user',
                }
            ).populate(
                {
                    path: 'guardian'
                }
            );
            return messageList;
        // res.status(200).json({error: null, resource: 'Message', results: messageList });
    }
    catch(err) {
        return(err);
    }
}

//Done
async function AllGuardianMessages(req, res, next) {
    try {
        console.log( req.userId);
        const allMesagges = await Message.find( { $or: [ {guardian:  req.userId}, {user:  req.userId} ] } ).populate(
                {
                    path: 'user'
                }
            ).populate(
                {
                    path: 'guardian'
                }
            );
        res.status(200).json({error: null, resource: 'Messages', results: allMesagges });
    }
    catch(err) {
        next(err);
    }
}

//Done
async function createMessage(req, res, next) {
    try {
        console.log(req.body);
        const messageList = await Message.create(req.body);
        res.status(200).json({error: null, resource: 'message', results: messageList });
    }
    catch(err) {
        if ( err.errors ) {
            res.status(400).json({error: true, resource: 'Error in values input', results: err.errors });
        }
        else if( err.keyValue ){
            res.status(409).json({error: true, resource: 'Duplicated Key', results: err.keyValue });
        }
        else {
            next(err);
        } 
    }
}

async function modifyMessage(req, res, next) {
    try {
        const messageList = await Message.find( { _id: req.params.messageId } )
        const messageNew = req.body.guardianMessages ? {guardianMessages : [...messageList[0].guardianMessages , req.body.guardianMessages]} : {userMessages : [...messageList[0].userMessages , req.body.userMessages]}
        console.log(messageNew);
        const messageUpdated = await Message.findByIdAndUpdate(req.params.messageId,  messageNew);
        res.status(200).json({ error: null, resource: 'message', results: 'messageUpdated' });
    }
    catch(err) {
        next(err);
    }
}

async function modifyMessageSocketIo(messageId, newMessage) {
    try {
        const messageList = await Message.find( { _id: messageId } )
        console.log(newMessage);
        // if(messageList[0].guardianMessages.lenght <1 && )
        const messageToAdd = newMessage.guardianMessages ? {guardianMessages : [...messageList[0].guardianMessages, newMessage.guardianMessages ]} : {userMessages : [...messageList[0].userMessages, newMessage.userMessages]}
        console.log(messageToAdd);
        const messageUpdated = await Message.findByIdAndUpdate(messageId,  messageToAdd);
        return newMessage
        // res.status(200).json({ error: null, resource: 'message', results: 'messageUpdated' });
    }
    catch(err) {
        return(err);
    }
}

async function modifyStatus(req, res, next) {
    try {
        await Message.findByIdAndUpdate(req.params.messageId,  req.body);
        res.status(200).json({ error: null, resource: 'message', results: 'Status Updated' });
    }
    catch(err) {
        next(err);
    }
}



async function deleteMessage(req, res, next) {
    try {
        await Message.findByIdAndDelete({ _id: req.params.messageId});
        res.status(200).json({error: null, resource: 'message', results: "message Deleted" });
    }
    catch(err) {
        next(err);
    }
}


module.exports = {
    oneMessage,
    createMessage,
    deleteMessage,
    modifyMessage,
    AllGuardianMessages,
    modifyStatus,
    oneMessageSocketIo,
    modifyMessageSocketIo,
}