const { find } = require('../models/User');
const User = require('../models/User');
const bcrypt = require('bcrypt');


async function listUser(req, res, next) {
    try {
        // console.log(req.params);
        const userList = await User.find({_id: req.userId});
        res.status(200).json({error: null, resource: 'userController', results: userList });
    }
    catch(err) {
        next(err);
    }
}



async function modifyUser(req, res, next) {
    try {
        console.log('Changing Data');
        console.log(req.body);
        console.log(req.userId);
        console.log('Modifying the data');
        const userUpdated = await User.findByIdAndUpdate(req.userId, req.body);
        // const userUpdated = await User.findById(req.userId )
        console.log(userUpdated);
        res.status(200).json({error: null, resource: 'userController', results: "user Updated correctly" });
    }
    catch(err) {
        next(err);
    }
}


async function deleteUser(req, res, next) {
    try {
        await User.findByIdAndDelete({ _id: req.userId});
        res.status(200).json({error: null, resource: 'userController', results: "user Deleted" });
    }
    catch(err) {
        next(err);
    }
}


module.exports = {
    deleteUser,
    modifyUser,
    listUser,
}