const logger = require("winston");
const User = require('../models/User');
const Post = require("../models/Post");

const cloudinary = require('../loaders/coudinaryConfig');


async function uploadToCloudinary(req, res, next) {
    try {
        const uploadedFile = await cloudinary('imgEne', req.file.buffer);
        req.imgUrl = {
            name: uploadedFile.originalname,
            url: uploadedFile.url,
        };
        next();
    }
    catch(err) {
        console.log('Internal error 500 en Upload to Cloudinary');
        next(err);
    }
}

async function uploadImageURL(req, res,next) {
    console.log("image: " + req.imgUrl.url);
    try{
        await User.findByIdAndUpdate(req.userId,{ image: req.imgUrl.url });
        res.status(200).json({error: null, resource: 'image', results: "image Updated" });
    }
    catch(err) {
        next(err);
    }
}

async function uploadPostImageURL(req, res,next) {
    console.log("image: " + req.imgUrl.url);
    try{
        const postToUpdate = await Post.find({_id: req.params.postId})
        const newPostImage = [ ...postToUpdate[0].images , req.imgUrl.url]
        console.log(newPostImage);
        await Post.findByIdAndUpdate( req.params.postId, { images: newPostImage });
        res.status(200).json({error: null, resource: 'Post image', results: "Post Image Updated" });
    }
    catch(err) {
        next(err);
    }
}

async function returnPostImageURL(req, res,next) {
    console.log("image: " + req.imgUrl.url);
    try{
        // const postToUpdate = await Post.find({_id: req.params.postId})
        // const newPostImage = [ ...postToUpdate[0].images , req.imgUrl.url]
        // console.log(newPostImage);
        // await Post.findByIdAndUpdate( req.params.postId, { images: newPostImage });
        res.status(200).json({error: null, resource: 'Post image', results: req.imgUrl.url});
    }
    catch(err) {
        next(err);
    }
}




module.exports = {
    uploadToCloudinary,
    uploadImageURL,
    uploadPostImageURL,
    returnPostImageURL,
};