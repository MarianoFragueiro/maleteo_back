const Post = require('../models/Post');



async function listUserPosts(req, res, next) {
    try {
        // console.log("UserId: " + req.body.userId);
        // console.log(req.query);
        const filter = {};
        if (req.query.s) {
            filter.name = new RegExp('.*' + req.query.s + '.*');
        }
        console.log(req.userId);
        const postList = await Post.find({ user: req.userId }).populate(
                {
                    path: 'user'
                }
            );
        console.log(postList);
        res.status(200).json({error: null, resource: 'post', results: postList });
    }
    catch(err) {
        next(err);
    }
}

async function onePost(req, res, next) {
    try {

        // console.log(req.params.postId);
        const postSearch = await Post.find({ _id: req.params.postId }).populate(
                {
                    path: 'user'
                }
            );
        // console.log(postSearch);
        res.status(200).json({error: null, resource: 'post', results: postSearch });
    }
    catch(err) {
        next(err);
    }
}



async function AllPost(req, res, next) {
    try {
        // console.log("UserId: " + req.body.userId);
        // console.log(req.query);
        // const filter = {};
        // if (req.query.s) {
        //     filter.name = new RegExp('.*' + req.query.s + '.*');
        // }
        const allPosts = await Post.find().populate(
                {
                    path: 'user'
                }
            );
        // console.log(allPosts);
        res.status(200).json({error: null, resource: 'post', results: allPosts });
    }
    catch(err) {
        next(err);
    }
}


async function createPost(req, res, next) {
    try {
        req.body.user = req.userId
        console.log(req.body);
        req.body.user.userId;
        const postList = await Post.create(req.body);
        res.status(200).json({error: null, resource: 'post', results: postList });
    }
    catch(err) {
        if ( err.errors ) {
            res.status(400).json({error: true, resource: 'Error in values input', results: err.errors });
        }
        else if( err.keyValue ){
            res.status(409).json({error: true, resource: 'Duplicated Key', results: err.keyValue });
        }
        else {
            next(err);
        } 
    }
}

async function modifyPost(req, res, next) {
    try {
        // console.log(req.params.postId);
        const postUpdated = await Post.findByIdAndUpdate(req.params.postId, req.body);
        // console.log(postUpdated);
        res.status(200).json({error: null, resource: 'post', results: postUpdated });
    }
    catch(err) {
        next(err);
    }
}



async function deletePost(req, res, next) {
    try {
        await Post.findByIdAndDelete({ _id: req.params.postId});
        res.status(200).json({error: null, resource: 'post', results: "post Deleted" });
    }
    catch(err) {
        next(err);
    }
}


module.exports = {
    listUserPosts,
    // replacePost,
    createPost,
    deletePost,
    modifyPost,
    AllPost,
    onePost
}