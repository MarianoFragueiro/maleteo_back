const logger = require('winston');

const authController = require('../controllers/authController');
const publicRoutes = require('../routes/publicRoutes');
const privateRoutes = require('../routes/privateRoutes');

function loader(app) {
    
    app.use(publicRoutes);
    
    app.use(authController.verifyToken, privateRoutes);
    // app.use( privateRoutes);

    app.use(function (req, res) {
        res.status(404).json({ error: 'Ruta no encontrada sssory' });
    });

    app.use(function (err, req, res, next) {
        logger.debug(err.stack);
        console.log(err.stack);
        res.status(500).json({ error: err.message });
    });
}

module.exports = loader;
