const passport = require('passport');


const passportLocal = require('./passportStrategies/passportLocal');
const passportGoogle = require('./passportStrategies/passportGoogle');
const passportFacebook = require('./passportStrategies/passportFacebook');



function loader(app){
    
    passport.use('registerLocal', passportLocal.registerLocal)
    passport.use('loginLocal', passportLocal.loginLocal)

    passport.use('registerGoogle', passportGoogle.registerStrategy);
    passport.use('loginGoogle', passportGoogle.loginStrategy);

    passport.use('registerFacebook', passportFacebook.registerStrategy);
    passport.use('loginFacebook', passportFacebook.loginStrategy);

    app.use(passport.initialize());
}


module.exports =  loader;
