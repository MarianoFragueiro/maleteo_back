const logger = require('winston');

const path = require('path')
const fs = require('fs')

const socketio = require('socket.io')
const https = require('https')

const config = require('../config');
const passportLoader = require('./passportLoader');
const mongooseLoader = require('./mongooseLoader');
const requestRoutingLoader = require('./requestRoutingLoader');
const requestProcessLoader = require('./requestProcessLoader');

// const certOptions = {
//     key: fs.readFileSync(path.resolve('certs/server.key')),
//     cert: fs.readFileSync(path.resolve('certs/server.crt'))
//   }

// v 

async function loader(app){

    try{
        await mongooseLoader();
        logger.info(`Database is running on mongodb://127.0.0.1:27017/maleteo`);
        
        // const server = https.createServer(options, app)
        // const io = socketio(server)
        // app.set('io', io)

        requestProcessLoader(app);
        passportLoader(app);
        requestRoutingLoader(app);
        logger.info('Express ready')
    }
    catch(err){
        throw err;
    }
}

module.exports = loader;