const mongoose = require('mongoose');

const config = require('../config');

const DB_URI = 'mongodb://127.0.0.1:27017/maleteo';

function loader(){
    return mongoose.connect(DB_URI,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        },
    )
}

module.exports = loader;