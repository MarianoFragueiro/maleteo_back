const express = require('express');

const config = require('../config');
const multer = require('./multerLoader');

function loader(app) {

    app.use(express.json());

    app.use(multer().single(config.server.images.multer.imageFieldName));
}

module.exports = loader;