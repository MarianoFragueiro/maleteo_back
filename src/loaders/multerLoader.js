const multer = require('multer');

function loader() {
    console.log('Enter to multer');
    return multer({
        storage: multer.memoryStorage(),//Pasar imagen a binario, la guardo en local
    });
}

module.exports = loader;
