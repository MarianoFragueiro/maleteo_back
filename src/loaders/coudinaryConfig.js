const cloudinary = require('cloudinary').v2;
const streamifier = require('streamifier');

const config = require('../config');

function loader(folderName, fileData) {
    console.log('Enter cloudinary loader');
    cloudinary.config(
        {
            cloud_name: config.server.images.cloudinary.cloud_name, 
            api_key: config.server.images.cloudinary.api_key , 
            api_secret: config.server.images.cloudinary.api_secret 
    });
    console.log('After server configurations');
    return new Promise(function(resolve, reject) {
        streamifier.createReadStream(fileData).pipe(
            cloudinary.uploader.upload_stream({folder: folderName}, function(err, file) {
                if (err) {
                    console.log('Error en Cloud');
                    reject(err);
                }
                resolve(file);
            }),
        );
    });
}

module.exports = loader;