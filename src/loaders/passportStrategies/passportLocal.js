const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../../models/User');

const registerLocal = new LocalStrategy(
    {
        usernameField: 'email',
        passReqToCallback: true,
    },
    async function(req, email, password, done) {
        try {
            console.log("registerLocal");
            const newUser = await User.create(req.body);
            req.userId = newUser._id;
            newUser.password = await bcrypt.hash(newUser.password, 10);
            newUser.save()
            done(null, true);
        }
        catch(err) {
            if ( err.errors ) {
                req.errors = err.errors;
                done(null, true);
            }
            else if( err.keyValue ){
                req.duplicatedKey = err.keyValue;
                console.log(err.keyValue);
                done(null, true);
            }
            else {
                done(err);
            }    
        }
    }
);

const loginLocal = new LocalStrategy(
    {
        usernameField: 'email',
        passReqToCallback: true,
    },
    async function (req, email, password, done) {
        try {
            console.log('Login User Manual');
            const userFound = await User.findOne({email: req.body.email});
            if (userFound) {
                if (await userFound.checkPassword(req.body.password)) {
                    console.log('User Found');
                    req.userId = userFound._id;
                    done(null, true);
                }
                else {
                    done(null, false);
                }
            }
            else {
                req.userNotFound = "The user doesn't exists";
                done(null, true);
            }
        }
        catch(err) {
            done(err);
        }
    }
);


module.exports = {
    registerLocal,
    loginLocal,
};