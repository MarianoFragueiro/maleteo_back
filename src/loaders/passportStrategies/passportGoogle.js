const GoogleStrategy = require('passport-google-oauth20').Strategy;
const config = require('../../config');
const User = require('../../models/User');

const registerStrategy = new GoogleStrategy(
    {
        clientID: config.server.logginGoogle.GoogleID,
        clientSecret: config.server.logginGoogle.GoogleClientSecret,
        callbackURL: 'http://localhost:5050/register/google/cb',
        passReqToCallback: true,
    },
    async function (req, accessToken, refreshToken, profile, done) {

        console.log('register Google');
        try{
            const data = {
            "email": profile.emails[0].value,
            "surname": profile.name.familyName,
            "name": profile.name.givenName
        };
        const newUser = await User.create(data)
        req.userId = newUser._id;
        req.email = newUser.email
        console.log(newUser);
        done(null, true);
        }
        catch(err) {
            if ( err.errors ) {
                req.errors = err.errors;
                done(null, true);
            }
            else if( err.keyValue ){
                req.duplicatedKey = err.keyValue;
                console.log(err.keyValue);
                done(null, true);
            }
            else {
                done(err);
            }    
        }
    }
);

const loginStrategy = new GoogleStrategy(
    {
        clientID: config.server.logginGoogle.GoogleID,
        clientSecret: config.server.logginGoogle.GoogleClientSecret,
        callbackURL: 'http://localhost:5050/login/google/cb',
        passReqToCallback: true,
    },
    async function(req, accessToken, refreshToken, profile, done) {
        try {
            const email = profile.emails[0].value;
            const userFound = await User.findOne({email: email});
            if (userFound) {
                req.userId = userFound._id;
                req.email = userFound.email
                done(null, true);
            }
            else {
                req.userNotFound = "The user doesn't exists";
                done(null, true);
            }
            
        }
        catch(err) {
            done(err);
        }
    }
);

module.exports = {
    registerStrategy,
    loginStrategy,
};
