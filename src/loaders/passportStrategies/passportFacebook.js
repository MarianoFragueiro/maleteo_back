const FacebookStrategy = require('passport-facebook').Strategy;
const config = require('../../Config');
const User = require('../../models/User');

const registerStrategy = new FacebookStrategy(
    {
        clientID: '479439419907004',
        clientSecret: 'ee2547d7c998b90ad9edf4d462a2cb74',
        callbackURL: "http://localhost:5050/register/facebook/callback",
        profileFields:['id', 'displayName', 'email'],
        passReqToCallback: true,
    },
    async function(req, accessToken, refreshToken, profile, done) {  
        try{
            console.log(profile);
            const data = {
                email: profile.emails[0].value,
                name: profile._json.name
            };
            // console.log(data);
            const newUser = await User.create(data)
            req.email= profile.emails[0].value;
            done(null, true);
        }
        catch(err) {
            if ( err.errors ) {
                req.errors = err.errors;
                done(null, true);
            }
            else if( err.keyValue ){
                req.duplicatedKey = err.keyValue;
                console.log(err.keyValue);
                done(null, true);
            }
            else {
                done(err);
            }    
        }
    },
);

const loginStrategy = new FacebookStrategy(
    {
        clientID: '479439419907004',
        clientSecret: 'ee2547d7c998b90ad9edf4d462a2cb74',
        callbackURL: "http://localhost:5050/login/facebook/callback",
        profileFields:['id', 'displayName', 'email'],
        passReqToCallback: true,
    },
    async function(req, accessToken, refreshToken, profile, done) {   
        try {
            
            const email = profile.emails[0].value;
            const userFound = await User.findOne({email: email});
            if (userFound) {
                req.email = email;
                req.userId = userFound._id;
                done(null, true);
            }
            else {
                req.userNotFound = "The user doesn't exists";
                done(null, true);
            }
            
        }
        catch(err) {
            done(err);
        }
    }
);



module.exports = {
    registerStrategy,
    loginStrategy
}