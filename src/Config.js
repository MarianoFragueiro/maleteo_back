const path = require('path');
const winston = require('winston');

const config = {
    server: {
        protocol: process.env.PROTOCOL || 'http',
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 6500,
        
        logginGoogle:{
            GoogleID: process.env.GOOGLE_CLIENT_ID || '244085578920-bobj6me5ek7ji7fos3g5n6fh8inmrpns.apps.googleusercontent.com',
            GoogleClientSecret: process.env.GOOGLE_CLIENT_SECRET || 'UfwQKQZ06ogqhIfMpXt2vXD1',
        },
        logginFacebook:{
            FacebookID:  '479439419907004' || process.env.FACEBOOK_APP_ID ,
            FacebookSecret:  'ee2547d7c998b90ad9edf4d462a2cb74' || process.env.FACEBOOK_APP_SECRET ,
        },
        images: {
            multer: {
                imageFieldName: process.env.MULTER_SINGLE_IMG_NAME || 'image',
            },
            cloudinary: {
                cloud_name: 'ddiy4zt1u' || process.env.CLOUDINARY_NAME  ,
                api_key: '397514982814783' || process.env.CLOUDINARY_KEY ,
                api_secret: 'R4wXhiuuRwQyECdgpHFcZa9B5eA' || process.env.CLOUDINARY_SECRET,
            },
        },
    },
    db: {
        protocol: process.env.DB_PROTOCOL || 'mongodb',
        host: process.env.DB_HOST || '127.0.0.1',
        port: process.env.DB_PORT || '27017',
        name: process.env.DB_NAME || 'maleteo',
    },
};

module.exports = config;