const express = require('express');
const cors = require('cors');
const messageController = require('./controllers/messageController');
const authController = require('./controllers/authController')

const socketIo = require("socket.io");

const config = require('./config');
const loaders = require('./loaders/indexLoader');

const app = express();

app.use(cors());
const server = app.listen(5050, function () {
    loaders(app);
}); 


const io = socketIo(server, { 
    cors: {
        origin: "http://localhost:3000",
        credentials : true
    }
});

let dataRecive = {};


  io.on("connection", (socket) => {

      socket.on("messageId", (data) => {
      console.log("Receive From Web data");
      // console.log(data);
      // console.log('All Messages',data.allMessages);
      saveMessageOnDB(socket , data)
      // getMessageId(socket , data)
    });

      socket.on("GoogleOrFacebookTokenRequest", (data) => {
      console.log("Receive From Web data");
      console.log(data);
      dataRecive = data
      sendToken(socket , data)
      // getMessageId(socket , data)
    });

      socket.on("GoogleOrFacebookTokenRequestHome", () => {
      console.log("Receive From Web data");
      // console.log(data);
      data = dataRecive 
      getTokenReadyHome(socket , data)
      // getMessageId(socket , data)
    });
    

    //  interval = setInterval(() => getApiAndEmit(socket), 1000);
    socket.on("disconnect", () => {
      console.log("Client disconnected");
    });
  });
  
 
  async function saveMessageOnDB(socket, data){
    const res = await messageController.modifyMessageSocketIo(data._id ,data.dataToSend)
    console.log(res);
    // console.log('En MessageOnDB', data.allMessages);
    
    getApiAndEmit(socket, data._id, res, data.allMessages)
}

  async function sendToken(socket, data){
    getTokenReady(socket, data)
}


  const getApiAndEmit = (socket, data_id, newMessage, allMessages) => {
    // console.log(data_id, newMessage);
    // console.log('All Messages', allMessages);
    const sendData = {_id: data_id, newMessage}
    // socket.emit("FromAPI",  sendData);
    // socket.broadcast.emit("FromAPI",  sendData)

    socket.local.emit("FromAPI",  {sendData, allMessages})
    return
  };

  const getTokenReady = (socket, data) => {
    console.log(data);
    // const sendData = {_id: data_id, newMessage}
    socket.emit("GoogleOrFacebookTokenRequest",  data);
    return
  };
  const getTokenReadyHome = (socket, data) => {
    console.log(data);
    // const sendData = {_id: data_id, newMessage}
    socket.emit("GoogleOrFacebookTokenRequestHome",  data);
    return
  };


 
module.exports={
  io
}