const mongoose = require('mongoose');


const postSchema = new mongoose.Schema(
    {
        title:{
            type:String
        },
        description:{
            type:String 
        },
        user: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'User',
            // unique: true,
        },
        datesNotAvailable: [{
            type: String,
        }],
        latPosition: {
            type: String,
        },
        lngPosition: {
            type: String,
        },
        country: {
            type: String,
        },
        city:{
            type: String,
        },
        address:{
            type: String,
        },
        timeAvailable:[{
            type: String,
        }],
        images:[{
            type: String,
        }],
        price:{
            type:String
        },
        servicies:{
            type:String
        },
        location:{
            type: String,
        },
        home:{
            type: String,
        },
        space:{
            type: String,
        }
    },
    {
        timestamps: true,
    }
);





const Post = mongoose.model('Post', postSchema);

module.exports = Post;
