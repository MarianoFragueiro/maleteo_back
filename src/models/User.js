const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


const userSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true,
            unique: true,
        },
        password: {
            type: String,
        },
        name: {
            type: String,
        },
        surname: {
            type: String,
        },
        image: {
            type: String,
        },
        birthDate:{
            type:String
        },
        guardian:{
            type: Boolean
        }
    },
    {
        timestamps: true,
    }
);

// userSchema.pre('save', async function(done) {
//     try {
//         this.password = await bcrypt.hash(this.password, 10);
//         done();
//     }
//     catch(err) {
//         done(err);
//     }
// });

userSchema.method({
    // encryptPwd: function(done) {
    //     this.password = await bcrypt.hash(this.password, 10);
    //     console.log(this.password);
    //     return ;
       
    // },
    checkPassword: function(passwordPlainText) {
        return bcrypt.compare(passwordPlainText, this.password);
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
