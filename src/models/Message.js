const mongoose = require('mongoose');


const messageSchema = new mongoose.Schema(
    {
        title:{
            type: String,
            unique: true
        },
        user: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'User',
            // unique: true,
        },
        guardian: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'User',
            // unique: true,
        },
        userMessages:[{
            type: String,
        }],
        guardianMessages:[{
            type: String,
        }],
        date:[{
            type:String,
        }],
        status:{
            type:String,
        }
    },
    {
        timestamps: true,
    }
);

messageSchema.method({
    addMessage: function(messageToAdd) {
        return this.messages.push(messageToAdd);
    }
});



const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
