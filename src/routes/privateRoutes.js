const express = require('express');


const postController = require('../controllers/postController');
const userController = require('../controllers/userController');
const imageController = require('../controllers/imageController');
const messageController = require('../controllers/messageController');

const router = express.Router();


router.route('/post')
    .post(postController.createPost); 

router.route('/allposts')
    .get(postController.AllPost) 

router.route('/postsuser')
    .get(postController.listUserPosts) 

router.route('/post/:postId')
    .get(postController.onePost) 
    .patch(postController.modifyPost) 
    .delete(postController.deletePost); 

// router.route('/usermessages')
//     .post(messageController.AllMessages) 

router.route('/guardianmessages')
    .get(messageController.AllGuardianMessages) 

router.route('/message')
    .post(messageController.createMessage); 

router.route('/message/:messageId')
    .get(messageController.oneMessage) 
    .patch(messageController.modifyMessage) 
    .delete(messageController.deleteMessage); 

router.route('/changestatus/:messageId')
    .patch(messageController.modifyStatus) 
    
// router.route('/user')
//     .get(userController.listUser);


router.route('/user')
    .get(userController.listUser)
    .patch(userController.modifyUser) // Modificar un user 
    .delete(userController.deleteUser); // Eliminar un user 



router.route('/user/image')
    .post(imageController.uploadToCloudinary, imageController.uploadImageURL);

router.route('/post/image/:postId')
    .post(imageController.uploadToCloudinary, imageController.uploadPostImageURL);

router.route('/post/image')
    .post(imageController.uploadToCloudinary, imageController.returnPostImageURL);


module.exports = router;