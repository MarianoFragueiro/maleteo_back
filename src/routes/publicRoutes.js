const express = require('express');
const passport = require('passport');
const socketIo = require('../app');


const authController = require('../controllers/authController')

const router = express.Router();



router.route('/init')
    .post(authController.verifyToken, authController.loginActive);


router.route('/register')
    .post(passport.authenticate('registerLocal', { session: false }), authController.handleErrorLogin, authController.getTokenManual);
router.route('/login')
    .post(passport.authenticate('loginLocal', { session: false }), authController.handleErrorLogin ,authController.getTokenManual);


router.route('/register/google')
    .get(  passport.authenticate('registerGoogle', { scope: ['profile', 'email'] }));
router.route('/register/google/cb')
    .get(passport.authenticate('registerGoogle', { session: false }),authController.handleErrorLogin ,authController.getToken);

router.route('/login/google')
    .get( passport.authenticate('loginGoogle', { scope: ['profile', 'email'] }));
router.route('/login/google/cb')
    .get(passport.authenticate('loginGoogle', { session: false }), authController.handleErrorLogin ,authController.getToken);

router.route('/register/facebook')
    .get( passport.authenticate('registerFacebook', { scope: 'email' }));
router.route('/register/facebook/callback')
    .get(passport.authenticate('registerFacebook', { session: false }), authController.handleErrorLogin ,authController.getToken);

router.route('/login/facebook')
    .get( passport.authenticate('loginFacebook', { scope: ['profile', 'email'] }));
router.route('/login/facebook/callback')
    .get(passport.authenticate('loginFacebook', { session: false }), authController.handleErrorLogin ,authController.getToken);






module.exports = router;